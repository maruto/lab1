package m_gayfutdinov.lab1;
import java.util.Comparator;

class ComparatorTableRow implements Comparator {
    public int compare(Object o0, Object o1) {
        TableRow t0 = (TableRow) o0;
        TableRow t1 = (TableRow) o1;
        return t1.getCounter()- t0.getCounter();
    }
}
