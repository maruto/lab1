package m_gayfutdinov.lab1;

import java.io.*;
import java.util.*;
import java.lang.StringBuilder;

public class Main {
    public static void main(String[] args) {
        /* Проверяем корректность входных данных на колличесвто параметров */
        try {
            if (args.length != 2)
                throw new Exception("Invalid number of parameters. Two files are expected.");
        } catch (Exception ex){
            System.err.println(ex.getMessage());
            System.exit(0);
        }

        /*============================================================================================================*/
        try( Reader reader = new InputStreamReader(new FileInputStream(args[0]));
             FileWriter writer = new FileWriter(args[1], false) ) {

            HashMap <String, Integer> map; //словарь
            map = new HashMap<>();

            //пока есть строки - считываем
            int c;
            StringBuilder strBuilder = new StringBuilder();
            while((c=reader.read())!=-1) {
                if (Character.isLetterOrDigit((char)c)) {
                    strBuilder.append((char)c);
                }else {
                    if(strBuilder.toString().length() != 0) {
                        map.merge(strBuilder.toString().toLowerCase(), 1, (prev, one) -> prev + one);
                        strBuilder.delete(0, strBuilder.length());
                    }
                }
            }

            /*--------------------------------------------------------------------------------------------------------*/
            //переводим map в ArrayList
            int count = 0;
            ArrayList <TableRow> sortedMap;
            sortedMap = new ArrayList<>();

            for (Map.Entry<String, Integer> pair : map.entrySet()) {
                sortedMap.add(new TableRow(pair.getKey(), pair.getValue()));
                count += pair.getValue();
            }
            /*--------------------------------------------------------------------------------------------------------*/
            //сортируем и выводим
            ComparatorTableRow comparator = new ComparatorTableRow();
            sortedMap.sort(comparator);

            for (TableRow tmp : sortedMap) {
                tmp.setFrequency(count);
                writer.write(tmp.getRow() + "\n");
            }
            /*--------------------------------------------------------------------------------------------------------*/
        } catch(IOException ex){
            System.err.println(ex.getMessage());
        }
    }
}