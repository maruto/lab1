package m_gayfutdinov.lab1;


class TableRow {
    private final String word;
    private final int counter;
    private double frequency;

    TableRow( String word, int counter){
        this.word = word;
        this.counter = counter;
        this.frequency = 0.0;
    }

    int getCounter(){ return counter; }

    public void setFrequency(int count) { frequency = ((double)counter/(double)count)*100.0; }

    String getRow(){ return (word + ',' + counter + ',' + frequency); }
}
